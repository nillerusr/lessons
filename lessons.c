#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <string.h>
#include <stdbool.h>

#define LESSONS_NUM 10
int lcount; // lessons cout


static void skeleton_daemon()
{
	pid_t pid;
	pid = fork();

	if (pid < 0)
		exit(EXIT_FAILURE);

	if (pid > 0)
		exit(EXIT_SUCCESS);

	if (setsid() < 0)
		exit(EXIT_FAILURE);

	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, SIG_IGN);

	pid = fork();

	if (pid < 0)
		exit(EXIT_FAILURE);

	if (pid > 0)
		exit(EXIT_SUCCESS);

	umask(0);

	chdir("/");

	int x;
	for (x = sysconf(_SC_OPEN_MAX); x>=0; x--)
	{
		close (x);
	}
}


void vibrate( int time ) // in ms
{
	FILE *f;

	f = fopen("/sys/devices/virtual/timed_output/vibrator/enable","w"); // Open sysfs switch
	fprintf(f,"%d\n",time); // enable vibration
	fclose(f);
}


void timeadd( int *time, int min )
{
	int hour = min/60;
	time[0] = time[0] + hour;
	time[1] = time[1] + min-hour*60;

	if( time[1] >= 60 )
	{
		time[0]++;
		time[1] = time[1] - 60;
	}
	else if( time[1] < 0 )
	{
		time[0]--;
		time[1] = 60 + time[1];
	}

	if( time[0] >= 24 )
		time[0] -= (int)(time[0] / 24)*24;
	else if( time[0] < 0 )
		time[0] += (int)(time[0] / 24 + 1)*24;

}

int lessons[LESSONS_NUM*2][2];

int load_lessons( )
{
	int c,i,b;
	i=b=0;
	FILE *f = fopen("lessons.txt","r");
	char buf[16];
	buf[0]='\0';

	while((c = fgetc_unlocked(f)) != EOF)
	{
		if( c == ' ' )
			continue;

		if( c == ':' )
		{
			lessons[i][b] = atoi(buf);
			b++;
			buf[0]='\0';
			continue;
		}

		if( c == '\n' || c == '-' || c == ':' )
		{
			lessons[i][b] = atoi(buf);
			buf[0]='\0';
			i++;b=0;
			lcount++;
			continue;
		}

		sprintf(buf,"%s%c",buf,c);
	}

	lcount /= 2;

	fclose(f);
}


int main()
{
	int i;
	time_t rawtime;
	struct tm * timeinfo;

	load_lessons();
	for( i = 0; i < lcount*2;i++)
		timeadd(lessons[i],-3);

	skeleton_daemon();

	while( 1 )
	{
		time( &rawtime );
		timeinfo = localtime ( &rawtime );
		for(i=0; i < LESSONS_NUM*2;i++)
		{
			if( lessons[i][0] != 0 && lessons[i][0] == timeinfo->tm_hour && 
				lessons[i][1] == timeinfo->tm_min )
			{
				vibrate( 5000 );
				sleep(61);
			}
		}
		sleep(1);
	}

	return 0;
}
